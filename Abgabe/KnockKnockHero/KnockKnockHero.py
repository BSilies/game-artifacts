#!/usr/bin/env python

import pygame
import os
import threading
import time
import RPi.GPIO as GPIO
import requests
import json
from multiprocessing.dummy import Pool

class Clapper(threading.Thread):
    '''
    Clapper runs in its own Thread and claps one part from the song "Clapping Music" by Steve Reich.
    For each clap a HTTP-request is sent to the endpoint "/knock" of a customizable ip-Adress.

    Parameters:
        threadID (int): ID of the Thread.
        variant (int): 1 or 2. Defines which part of the song shall be played.
            1: the steady part
            2: the shifting part
        sound (str): path of the soundfile that shall be played.
        ip (str): IP-adress of the station that shall receive clapping-Information
    '''
    def __init__(self, threadID, variant, sound, ip):
        threading.Thread.__init__(self)
        self.THIS_FOLDER = os.path.dirname(os.path.abspath(__file__))
        self.threadID = threadID
        self.variant = variant
        self.sound = os.path.join(self.THIS_FOLDER, sound)
        self.ip = ip

        # Clapping Sequenz
        self.sequence = [1,1,1,0,1,1,0,1,0,1,1,0]

        # Thread-pool for HTTP-Requests
        self.pool = Pool(4)

        pygame.init()
        pygame.mixer.init()
        self.sound_play = pygame.mixer.Sound(self.sound)

    def run(self):
        print("start thread")
        delay = 0.75
        # break between two sequence-steps
        delay_double = 2*delay

        while True:
            # check whether the clapping shall be active
            while GPIO.input(inputPin):
                i = 0
                # repetitions before shifting - in the original song there are four
                while i < 1:
                    # break if clapping shall not be active
                    if not GPIO.input(inputPin):
                        break
                    for status in self.sequence:
                        # break if clapping shall not be active
                        if not GPIO.input(inputPin):
                            break
                        # if current status in the sequence is 1, play a clapping sound and send a request
                        if status:
                            pygame.mixer.Sound.play(self.sound_play)
                            # send request in its own thread to not block the main-clapping-loop by waiting for the unneeded response
                            self.pool.apply_async(requests.get, ['http://'+self.ip+'/knock'])
                            # wait a certain time to let the station process the request
                            time.sleep(delay_double)
                        else:
                            # wait to stay in timing
                            time.sleep(delay_double)
                    i += 1
                # shift left the sequence if the shifting part is played by this Thread
                if self.variant == 2:
                    element = self.sequence[0]
                    self.sequence.pop(0)
                    self.sequence.append(element)
            # reset the sequence if clapping is inactive
            self.sequence = [1,1,1,0,1,1,0,1,0,1,1,0]

# this pin decides whether clapping shall be active or not
inputPin = 23

GPIO.setmode(GPIO.BCM)
# set pill-up-resistor, so default-behaviur is active clapping
GPIO.setup(inputPin, GPIO.IN, pull_up_down = GPIO.PUD_UP)

# ips of the stations
ip1 = '192.168.4.1:80'
ip2 = '192.168.4.2:80'

# Thread-initializing
thread1 = Clapper(1,1,'clap1.wav',ip1)
thread2 = Clapper(2,2,'clap2.wav',ip2)

lastCall = time.time()
# polling-interval
interval = 5

result1 = 0
result2 = 0

# minimum needed knocks for success. Both configured stations need to reach at least this count of knocks in the same polling-interval
neededKnocks = 5

# start the threads
thread1.start()
thread2.start()

while True:
    # check whether it is time for polling
    if time.time() - lastCall >= interval:
        # try getting the current number of counts from station 1
        try:
            # get response from HTTP-call
            response1 = requests.get('http://'+ip1+'/count')
            # get response-body
            body1 = response1.text
            # transform body to JSON
            json1 = json.loads(body1)
            # get value from JSON
            result1 = json1['count']
            print('result1: '+ str(result1))
        except:
            print('Verbindung konnte nicht hergestellt werden')

        # try getting the current number of counts from station 2
        try:
            # get response from HTTP-call
            response2 = requests.get('http://'+ip2+'/count')
            # get response-body
            body2 = response2.text
            # transform body to JSON
            json2 = json.loads(body2)
            # get value from JSON
            result2 = json2['count']
            print('result2: '+ str(result2))
        except:
            print('Verbindung konnte nicht hergestellt werden')

        # check whether both stations reached at least ne minimum number of knocks and open the pocket on station 2
        if result1 >= neededKnocks and result1 >= neededKnocks:
            requests.get('http://'+ip2+'/open')
        # remember last polling time
        lastCall = time.time()
