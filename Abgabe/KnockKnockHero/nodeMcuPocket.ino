#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <Adafruit_NeoPixel.h>
#include <Servo.h>

#ifdef __AVR__
 #include <avr/power.h> // Required for 16 MHz Adafruit Trinket
#endif

// Define the Pins for sensors and actors
#define LED_PIN 1
#define SENSOR_PIN 3
#define SERVO_PIN 15

// Number of LEDs in the NeoPixel-Ring attached to the ESP?
#define LED_COUNT 24

// Set servo-instance
Servo servo;

// Declare the NeoPixel strip object:
Adafruit_NeoPixel strip(LED_COUNT, LED_PIN, NEO_GRB + NEO_KHZ800);

// Credentials for the WiFi
const char* ssid = "KnockKnock";
const char* password = "herohero";

// Variable Initializing
unsigned long startTime = 0;
unsigned long timeoutKnock = 1000;
unsigned long timeoutNotify = 100;
bool knock, notify, knocked;
int counter, highest = 0;

volatile unsigned long alteZeit=0, entprellZeit=50;

// Set port of the Webserver to 80 (HTTP)
ESP8266WebServer server(80);

void setup() {
  // Reset Wifi
  WiFi.disconnect();

  // Use Tx and Rx as GPIO-Pins beacause of space-limitation on the circuit board
  pinMode(1, FUNCTION_3);
  pinMode(3, FUNCTION_3);

  #if defined(__AVR_ATtiny85__) && (F_CPU == 16000000)
    clock_prescale_set(clock_div_1);
  #endif

  // set pin-modes
  pinMode(SENSOR_PIN, INPUT);
  pinMode(LED_PIN, OUTPUT);
  pinMode(SERVO_PIN, OUTPUT);

  // initialize LED-ring and turn all LEDs off
  strip.begin();
  strip.clear();
  strip.show();
  strip.setBrightness(40);

  // initialize servo
  servo.attach(SERVO_PIN);
  servo.write(90);

  // set LEDs to white
  strip.fill(strip.Color(255,255,255));
  strip.show();
  // open the box to fill it
  servo.write(150);
  delay(200);
  servo.write(90);
  // wait 15 Seconds to have a chance to fill the box
  delay(15000);
  // set LEDs to yellow
  strip.fill(strip.Color(255,255,0));
  // unlock the locking-mechanism so you can close the box
  strip.show();
  servo.write(150);
  // wait for 5 seconds to give a chance to close the box. The last second the LEDs are switched off
  delay(4000);
  strip.clear();
  strip.show();
  delay(1000);
  // lock the box
  servo.write(90);

  // initialize knocking-interrupt
  attachInterrupt(digitalPinToInterrupt(SENSOR_PIN), doKnock, RISING);

  // set network-setting
  IPAddress ip(192, 168, 4, 2);
  IPAddress gateway(192, 168, 4, 1);
  IPAddress subnet(255, 255, 255, 0);
  WiFi.config(ip, gateway, subnet);
  WiFi.mode(WIFI_STA);

  // start WiFi
  WiFi.begin(ssid, password);
  // wait for WiFi-connection
  while (WiFi.status() != WL_CONNECTED) {
    // while waiting, let the LEDs flash blue
    strip.fill(strip.Color(0,0,255));
    strip.show();
    delay(200);
    strip.clear();
    strip.show();
    delay(300);
  }

  // define HTTP-endpoints
  server.on("/knock", handleKnock);
  server.on("/count", handleCount);
  server.on("/open", handleOpen);
  server.onNotFound(handleNotFound);

  // start the server
  server.begin();

  // switch on green LEDs for two seconds to signalize success
  strip.fill(strip.Color(0,255,0));
  strip.show();
  delay(2000);
  strip.clear();
  strip.show();
}

void loop() {
  // check whether there was a correct knock
  if(knock and knocked) {
    // update counters
    counter++;
    if(counter > highest) {
      highest = counter;
    }
    // set LEDs to green to signalize success
    strip.fill(strip.Color(0,255,0));
    strip.show();
    // set start-time for duration measurement
    startTime = millis();
    // update flags
    knock = false;
    notify = true;
    knocked = false;
  // else check whether there was a false knock
  } else if(not knock and knocked) {
    // set LEDs to red to signalize failure
    strip.fill(strip.Color(255,0,0));
    strip.show();
    // set start-time for duration measurement
    startTime = millis();
    // update flags
    knock = false;
    notify = true;
    knocked = false;
    // update counter
    counter = 0;
  }
  // check whether a needed knock timed-out
  if(knock and millis()-startTime >= timeoutKnock) {
    // set LEDs to red to signalize failure
    strip.fill(strip.Color(255,0,0));
    strip.show();
    // set start-time for duration measurement
    startTime = millis();
    // update flags
    knock = false;
    notify = true;
    // update counter
    counter = 0;
  }
  // check whether a succes or failure notification ended
  if(notify and millis()-startTime >= timeoutNotify) {
    // clear LEDs
    strip.clear();
    strip.show();
    // update flag
    notify = false;
  }
  // handle possible HTTP-requests
  server.handleClient();
}

// handle a knock-request
void handleKnock() {
  // send empty json as response with status 200
  server.send(200, "text/json", "{}");
  // set LEDs to warm white
  strip.fill(strip.Color(255,190,75));
  strip.show();
  // set start-time for duration measurement
  startTime = millis();
  // update flag
  knock = true;
}

// handle a count-request
void handleCount() {
  // build JSON-response with the highest count since the last request with status 200
  String response = "{\"count\":";
  response += highest;
  response += "}";
  // send response
  server.send(200, "text/json", response);
  // update counter
  highest = counter;
}

// handle open-request
void handleOpen() {
  // send empty json as response with status 200
  server.send(200, "text/json", "{}");
  // unlock the box
  servo.write(150);
  delay(500);
  servo.write(90);
}

// handle request for undefined endpoint
void handleNotFound() {
  server.send(404, "text/plain", "Not found");
}

//Interrupt if a knock was detected.
ICACHE_RAM_ATTR void doKnock() {
  // check time since last signal
  if((millis() - alteZeit) > entprellZeit) {
    // set flag
    knocked = true;
    // save time for check sind last signal
    alteZeit = millis();
  }
}
