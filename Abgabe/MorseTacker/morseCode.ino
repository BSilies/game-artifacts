//Define inputs
#define button A5

//Define outputs
//debug outputs
#define led A4
#define relay 6

//Define variables
 
 //Defines the last state of the button. False == LOW ; True == HIGH
 //Used to define wether the button status changed.
 bool lastState = false;

 bool currentState = false;

 //Counter to estimated the time since the last button press
 int counter = 0;

 //Numbers to define the duration of the intervall
 int resetInterval = 100;
 int shortInterval = 20;
 int longInterval = 40;
 int spaceInterval = 100;

 //Defines the correct awnser
 //_ => long
 //. => short
 //P => Pause
 String correctAwnser = "-...P..P.P.-.";

 //Defines the current awnser
 String awnser = "";


void setup() {
 // put your setup code here, to run once:
 //set input pins
 pinMode(button, INPUT);

 //set output pins
 pinMode(led, OUTPUT);
 pinMode(relay, OUTPUT);

 
//Begin serial write for debug purposes
 Serial.begin (9600);
 Serial.println("Starting morse code application");
}

void loop()
{
  // put your main code here, to run repeatedly:
  
  //Output variables for debug purposes (slows down the program dramatically)
  Serial.print("Counter: ");
  Serial.println(counter);
  Serial.print("lastState: ");
  Serial.println(lastState);
  Serial.print("awnser: ");
  Serial.println(awnser);
  
  currentState = digitalRead(button);

  //Check if button state changed
  if(lastState != currentState)
  {
    //Check the length of the interval to add a new "word" / Pause
    if(counter > longInterval && counter <= spaceInterval)
    {
      //No pause at the beginning
      if(awnser != ""){
          awnser = awnser + "P";
      }
    }
    
    //Add current state to last state
    lastState = currentState;
    //if false
    if(!currentState)
    {
     //compare interval   
      if(counter <= shortInterval)
      {
        awnser = awnser + ".";
            
      } else if(counter > shortInterval && counter <= longInterval)
      {
        awnser = awnser + "-";
      }
    }
    //reset counter
    counter = 0;
  }
  //add counter
 ++counter;
 //Reset counter if interval is too long
 if(counter >= resetInterval){

  //check awnser
  if(awnser != correctAwnser){
    //play entered awnser to show output
   playSection(awnser);
  }
    //reset interval and counter
    awnser = "";
    counter = 0;
  }
  //if awnser is correct
  if(awnser == correctAwnser)
  {
    delay(500);
    playSection("-...P.-..P.-P..-");
    //reset interval and counter
    awnser = "";
    counter = 0;
  }

}

//Function to play an awnser easily
void playSection(String temp){
  //for every character
   for(int i = 0; i <= temp.length(); i++){
      //short interval character
      if(temp[i] == '.'){
          digitalWrite(led, HIGH);
          delay(500);
          digitalWrite(led, LOW);
        //long interval character
      }  else if(temp[i] == '-') {
          digitalWrite(led, HIGH);
          delay(1000);
          digitalWrite(led, LOW);
      }  else if(temp[i] == 'P') {
          delay(1500);
      }
      delay(500);
    }
}
