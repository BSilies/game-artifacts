# Konzeption Game Artifacts

## Thema des Escape Rooms:
- Büro (Neuzeit)
- verrückter Wissenschaftler (hat Corona entwickelt)
- (Preper-)Bunker mit Versuchsperson/Patient 0

## Ideen (favorisiert)
- Morse-Code-Taster mit Lampe/Summer etc. &rarr; Antwort bei korrekter Eingabe liefert weiteren Teil
  ([Doku](Doku/Morsecode.md))
- Knock-Knock Hero &rarr; Team muss gemeinsam Tasten zu einem Lied drücken ([Doku](Doku/KnockKnockHero.md))

----

[Weitere Ideen](Doku/Ideenpool.md)

[Referenzen](Doku/Referenzen.md)
