#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <Adafruit_NeoPixel.h>

#ifdef __AVR__
 #include <avr/power.h> // Required for 16 MHz Adafruit Trinket
#endif

// Define the Pins for sensors and actors
#define LED_PIN 1
#define SENSOR_PIN 3

// Number of LEDs in the NeoPixel-Ring attached to the ESP?
#define LED_COUNT 24

// Declare the NeoPixel strip object:
Adafruit_NeoPixel strip(LED_COUNT, LED_PIN, NEO_GRB + NEO_KHZ800);

// Credentials for the WiFi
const char* ssid = "KnockKnock";
const char* password = "herohero";

// Variable Initializing
unsigned long startTime = 0;
unsigned long timeoutKnock = 1000;
unsigned long timeoutNotify = 100;
bool knock, notify, knocked;
int counter, highest = 0;

volatile unsigned long alteZeit=0, entprellZeit=50;

// Set port of the Webserver to 80 (HTTP)
ESP8266WebServer server(80);

void setup() {
  // Use Tx and Rx as GPIO-Pins beacause of space-limitation on the circuit board
  pinMode(1, FUNCTION_3);
  pinMode(3, FUNCTION_3);

  #if defined(__AVR_ATtiny85__) && (F_CPU == 16000000)
    clock_prescale_set(clock_div_1);
  #endif

  // set pin-modes
  pinMode(SENSOR_PIN, INPUT);
  pinMode(LED_PIN, OUTPUT);

  // initialize LED-ring and turn all LEDs off
  strip.begin();
  strip.clear();
  strip.show();
  strip.setBrightness(40);

  // Reset Wifi
  WiFi.disconnect();

  // create WiFi-accesspoint
  WiFi.softAP(ssid, password);
  delay(100);

  // define HTTP-endpoints
  server.on("/knock", handleKnock);
  server.on("/count", handleCount);
  server.onNotFound(handleNotFound);

  // initialize knocking-interrupt
  attachInterrupt(digitalPinToInterrupt(SENSOR_PIN), doKnock, RISING);

  // start the server
  server.begin();

  // switch on green LEDs for two seconds to signalize success
  strip.fill(strip.Color(0,255,0));
  strip.show();
  delay(2000);
  strip.clear();
  strip.show();
}

void loop() {
  // check whether there was a correct knock
  if(knock and knocked) {
    // update counters
    counter++;
    if(counter > highest) {
      highest = counter;
    }
    // set LEDs to green to signalize success
    strip.fill(strip.Color(0,255,0));
    strip.show();
    // set start-time for duration measurement
    startTime = millis();
    // update flags
    knock = false;
    notify = true;
    knocked = false;
    // else check whether there was a false knock
  } else if(not knock and knocked) {
    // set LEDs to red to signalize failure
    strip.fill(strip.Color(255,0,0));
    strip.show();
    // set start-time for duration measurement
    startTime = millis();
    // update flags
    knock = false;
    notify = true;
    knocked = false;
    // update counter
    counter = 0;
  }
  // check whether a needed knock timed-out
  if(knock and millis()-startTime >= timeoutKnock) {
    // set LEDs to red to signalize failure
    strip.fill(strip.Color(255,0,0));
    strip.show();
    // set start-time for duration measurement
    startTime = millis();
    // update flags
    knock = false;
    notify = true;
    // update counter
    counter = 0;
  }
  // check whether a succes or failure notification ended
  if(notify and millis()-startTime >= timeoutNotify) {
    // clear LEDs
    strip.clear();
    strip.show();
    // update flag
    notify = false;
  }
  // handle possible HTTP-requests
  server.handleClient();
}

// handle a knock-request
void handleKnock() {
  // send empty json as response with status 200
  server.send(200, "text/json", "{}");
  // set LEDs to warm white
  strip.fill(strip.Color(255,190,75));
  strip.show();
  // set start-time for duration measurement
  startTime = millis();
  // update flag
  knock = true;
}

// handle a count-request
void handleCount() {
  // build JSON-response with the highest count since the last request with status 200
  String response = "{\"count\":";
  response += highest;
  response += "}";
  // send response
  server.send(200, "text/json", response);
  // update counter
  highest = counter;
}

// handle request for undefined endpoint
void handleNotFound() {
  server.send(404, "text/plain", "Not found");
}

//Interrupt if a knock was detected.
ICACHE_RAM_ATTR void doKnock() {
  // check time since last signal
  if((millis() - alteZeit) > entprellZeit) {
    // set flag
    knocked = true;
    // save time for check sind last signal
    alteZeit = millis();
  }
}
