import pygame
import pygame.mixer
import os
import numpy as np
import RPi.GPIO as GPIO

GPIO.setmode(GPIO.BMC)
GPIO.setup(23, GPIO.OUT)

THIS_FOLDER = os.path.dirname(os.path.abspath(__file__))
my_file = os.path.join(THIS_FOLDER, 'badGuy.wav')
my_csv_file = os.path.join(THIS_FOLDER, 'badGuy.csv')
my_meta_file = os.path.join(THIS_FOLDER, 'badGuyMeta.csv')

knocks = np.genfromtxt(my_csv_file, delimiter=',')
metadata = np.genfromtxt(my_meta_file, delimiter=',')

def get_knock(target_time):
    return bool(knocks[int(target_time*metadata)])

pygame.init()


pygame.mixer.music.load(my_file)
pygame.mixer.music.set_endevent(pygame.USEREVENT)
pygame.mixer.music.play(0)

running = True
on = False
while running:

    t = pygame.mixer.music.get_pos()/1000.0
    active = get_knock(t)
    if active and not on:
        GPIO.output(23, GPIO.HIGH)
        on = True
    elif not active and on:
        GPIO.output(23, GPIO.LOW)
        on = False

    # Did the user click the window close button?
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
        if event.type == pygame.USEREVENT:
            pygame.mixer.music.stop()
            pygame.mixer.music.play(0)

# Done! Time to quit.
pygame.quit()
