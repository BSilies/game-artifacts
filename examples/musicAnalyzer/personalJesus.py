import musicAnalyzerPrepper
import numpy as np
import os

def clean(track):
    resultInner = np.array([])
    buffer = np.array([])
    for k in range(len(track)):
        if track[k]:
            resultInner = np.append(resultInner, buffer)
            buffer = np.array([])
            resultInner = np.append(resultInner, [True])
        else:
            if (len(buffer) < 4 and len(buffer) > 0) or track[k-1]:
                buffer = np.append(buffer, [True])
            elif len(buffer) > 0:
                resultInner = np.append(resultInner, np.zeros(len(buffer)))
                resultInner = np.append(resultInner, [False])
                buffer = np.array([])
            else:
                resultInner = np.append(resultInner, [False])
    return resultInner

def cleanBeats(trackBeats):
    resultBeats = np.array([])
    bufferBeats = np.array([])
    for m in range(len(trackBeats)):
        if trackBeats[m] and m >= 2:
            resultBeats = np.append(resultBeats, bufferBeats)
            bufferBeats = np.array([])
            resultBeats = np.append(resultBeats, [True])
            resultBeats[m-1] = True
            resultBeats[m-2] = True
        elif trackBeats[m]:
            resultBeats = np.append(resultBeats, [True])
        elif m >= 1 and ((not trackBeats[m] and trackBeats[m-1]) or (len(bufferBeats) > 0 and len(bufferBeats) < 2)):
            bufferBeats = np.append(bufferBeats, [True])
        else:
            resultBeats = np.append(resultBeats, bufferBeats)
            bufferBeats = np.array([])
            resultBeats = np.append(resultBeats, [False])
    return resultBeats

tracks = [[170, 210, 50, 0], [270, 300, 50, 0], [0, 0, 0.12, 2]]
songname = 'personalJesus'

prepResult, metadata = musicAnalyzerPrepper.prep(songname, tracks)

bass = np.logical_or(clean(prepResult[0]), clean(prepResult[1]))
hits = cleanBeats(prepResult[2])

result = np.array([])
tempResult = np.array([], dtype='b')
beat = False
for i in range(len(bass)):
    if bass[i]:
        tempResult = np.append(tempResult, [True])
        beat = beat or hits[i]
    elif not bass[i] and beat:
        if(len(result) != 0):
            result = np.append(result, tempResult)
        else:
            result = tempResult
        beat = False
        tempResult = np.array([], dtype='b')
        result = np.append(result, [False])
    else:
        if(len(result) != 0):
            result = np.append(result, np.zeros(len(tempResult)))
        else:
            result = np.zeros(len(tempResult))
        result = np.append(result, [False])

THIS_FOLDER = os.path.dirname(os.path.abspath(__file__))

knockFile = os.path.join(THIS_FOLDER, songname+'.csv')
metaFile = os.path.join(THIS_FOLDER, songname+'Meta.csv')

np.savetxt(knockFile, hits, delimiter=",")
np.savetxt(metaFile, metadata, delimiter=",")
