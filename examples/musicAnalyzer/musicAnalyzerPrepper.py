import librosa
import librosa.display
import numpy as np
import os
import math

def get_decibel(freq_min, freq_max, spectrogram, freq_index_ratio, times, threshold):
    freq_min_index = int(freq_min*freq_index_ratio)
    freq_max_index = int(freq_max*freq_index_ratio)
    subset = spectrogram[freq_min_index:freq_max_index+1]
    decibels = np.zeros(len(times))
    for freq in subset:
        for i in range(len(freq)):
            if(freq[i]>(-80+threshold)):
                decibels[i] = 10*math.log10((10**(0.1*decibels[i]))+(10**(0.1*(freq[i]+80))))
    return decibels

def prep(songname, tracks, threshold = 40):
    THIS_FOLDER = os.path.dirname(os.path.abspath(__file__))
    songFile = os.path.join(THIS_FOLDER, songname+'.wav')

    y,sr = librosa.load(songFile)

    stft = np.abs(librosa.stft(y, hop_length=512, n_fft=2048*4))

    stft_harmonic, stft_percussive = librosa.decompose.hpss(stft, margin=8)

    spectrogram_harmonics = librosa.amplitude_to_db(stft_harmonic, ref=np.max)
    spectrogram_percussive = librosa.amplitude_to_db(stft_percussive, ref=np.max)

    onset_env = librosa.onset.onset_strength(y, sr=sr, aggregate=np.median)
    onset_env = librosa.util.normalize(onset_env)

    frequencies = librosa.core.fft_frequencies(n_fft=2048*4)
    times = librosa.core.frames_to_time(np.arange(spectrogram_harmonics.shape[1]), sr=sr, hop_length=512, n_fft=2048*4)

    time_index_ratio = len(times)/times[-1]
    frequencies_index_ratio = len(frequencies)/frequencies[-1]

    tempResult = np.array([], dtype='b')
    result = np.array([])

    for track in tracks:
        tempResult = np.array([], dtype='b')
        if(track[3] != 2):
            if track[3] == 1:
                decibels = get_decibel(track[0], track[1], spectrogram_percussive, frequencies_index_ratio, times, threshold)
            else:
                decibels = get_decibel(track[0], track[1], spectrogram_harmonics, frequencies_index_ratio, times, threshold)

            lastElement = 0
            for element in decibels:
                if element > track[2]+2 or (element > track[2] and element > lastElement):
                    tempResult = np.append(tempResult, [True])
                else:
                    tempResult = np.append(tempResult, [False])
                lastElement = element

            if(len(result) != 0):
                result = np.append(result, [tempResult], axis=0)
            else:
                result = [tempResult]
        else:
            for element in onset_env:
                if element > track[2]:
                    tempResult = np.append(tempResult, [True])
                else:
                    tempResult = np.append(tempResult, [False])

            if(len(result) != 0):
                result = np.append(result, [tempResult], axis=0)
            else:
                result = [tempResult]


    metadata = np.array([time_index_ratio])

    return result, metadata
