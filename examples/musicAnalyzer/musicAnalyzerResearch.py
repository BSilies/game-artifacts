import librosa
import librosa.display
import matplotlib.pyplot as plt
import numpy as np
import os

def prepare_spectrogram(original):
    copy = np.copy(original)
    for i in range(len(copy)):
        for j in range(len(copy[i])):
            if copy[i][j] < -40:
                copy[i][j] = -80
    return copy

songname = "badGuy8Bit"

THIS_FOLDER = os.path.dirname(os.path.abspath(__file__))
my_file = os.path.join(THIS_FOLDER, songname+'.wav')

try:
    stft = np.genfromtxt(songname+'Stft.csv', delimiter=',')
except:
    y,sr = librosa.load(my_file)

    stft = np.abs(librosa.stft(y, hop_length=512, n_fft=4096))

    np.savetxt(songname+"Stft.csv", stft, delimiter=",")

stft_harmonic, stft_percussive = librosa.decompose.hpss(stft, margin=8)

# onset_env = librosa.onset.onset_strength(y, sr=sr, aggregate=np.median)
# times = librosa.times_like(onset_env, sr=sr, hop_length=512)

spectrogram_original = librosa.amplitude_to_db(stft, ref=np.max)
# spectrogram = prepare_spectrogram(spectrogram_original)
spectrogram_harmonics_original = librosa.amplitude_to_db(stft_harmonic, ref=np.max)
# spectrogram_harmonics = prepare_spectrogram(spectrogram_harmonics_original)
spectrogram_percussive_original = librosa.amplitude_to_db(stft_percussive, ref=np.max)
# spectrogram_percussive = prepare_spectrogram(spectrogram_percussive_original)



# plt.subplot(311)
# librosa.display.specshow(spectrogram_original, y_axis='log', x_axis='time')
# # plt.xlim(0, 90)
# # plt.ylim(0,2048)
# plt.title('Original')
# plt.colorbar(format='%+2.0f dB')
# plt.tight_layout()

# plt.subplot(312)
# librosa.display.specshow(spectrogram_harmonics_original, y_axis='log', x_axis='time')
# # plt.xlim(0, 20)
# # plt.ylim(0,1024)
# plt.title('Harmonics')
# plt.colorbar(format='%+2.0f dB')
# plt.tight_layout()

# plt.subplot(313)
# librosa.display.specshow(spectrogram_percussive_original, y_axis='log', x_axis='time')
# # plt.xlim(0, 90)
# # plt.ylim(0,2048)
# plt.title('Percussives')
# plt.colorbar(format='%+2.0f dB')
# plt.tight_layout()

# plt.subplot(212)
# plt.plot(times, librosa.util.normalize(onset_env))
# plt.xlim(0, 25)
# plt.gca().xaxis.set_major_formatter(librosa.display.TimeFormatter())
# plt.tight_layout()

fig, ax = plt.subplots(nrows=3, sharex=True, sharey=True)

img = librosa.display.specshow(spectrogram_original, y_axis='hz', x_axis='time', ax=ax[0])
ax[0].set(title='Full spectrogram')
ax[0].label_outer()

librosa.display.specshow(spectrogram_harmonics_original, y_axis='hz', x_axis='time', ax=ax[1])
ax[1].set(title='Harmonic spectrogram')
ax[1].label_outer()

librosa.display.specshow(spectrogram_percussive_original, y_axis='hz', x_axis='time', ax=ax[2])
ax[2].set(title='Percussive spectrogram')

fig.colorbar(img, ax=ax)

plt.show()


