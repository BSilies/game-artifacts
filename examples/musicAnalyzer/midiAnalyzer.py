from mido import MidiFile

mid = MidiFile('personalJesus.mid')

print(mid)
print('\n')

for track in mid.tracks:
    print(track)
print('\n')

print(mid.length)
print('\n')

for msg in mid.tracks[2]:
    if(msg.type == 'note_on'):
        print(msg.velocity)
        print(msg.time)
        print('')
