import musicAnalyzerPrepper
import numpy as np
import os

def clean(track):
    resultInner = np.array([])
    buffer = np.array([])
    for k in range(len(track)):
        if track[k]:
            resultInner = np.append(resultInner, buffer)
            buffer = np.array([])
            resultInner = np.append(resultInner, [True])
        else:
            if (len(buffer) < 3 and len(buffer) > 0) or track[k-1]:
                buffer = np.append(buffer, [True])
            elif len(buffer) > 0:
                resultInner = np.append(resultInner, np.zeros(len(buffer)))
                resultInner = np.append(resultInner, [False])
                buffer = np.array([])
            else:
                resultInner = np.append(resultInner, [False])
    return resultInner

tracks = [[35, 105, 65, 0], [105, 110, 52, 0], [50, 100, 65, 1]]
songname = 'badGuy8Bit'

prepResult, metadata = musicAnalyzerPrepper.prep(songname, tracks)

bass = np.logical_or(clean(prepResult[0]), clean(prepResult[1]))
beat = clean(prepResult[2])

THIS_FOLDER = os.path.dirname(os.path.abspath(__file__))

knockFile = os.path.join(THIS_FOLDER, songname+'.csv')
metaFile = os.path.join(THIS_FOLDER, songname+'Meta.csv')

np.savetxt(knockFile, np.append([bass], [beat], axis=0), delimiter=",")
np.savetxt(metaFile, metadata, delimiter=",")
