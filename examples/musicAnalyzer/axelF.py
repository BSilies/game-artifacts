import musicAnalyzerPrepper
import numpy as np
import os

tracks = [[240, 275, 60, 0], [290, 325, 63, 0], [325, 375, 68, 0], [375, 400, 63, 0], [400, 430, 70, 0], [450, 480, 65, 0], [500, 535, 65, 0], [540, 570, 63, 0], [680, 720, 65, 0]]
songname = 'axelF'

result, metadata = musicAnalyzerPrepper.prep(songname, tracks)

THIS_FOLDER = os.path.dirname(os.path.abspath(__file__))

knockFile = os.path.join(THIS_FOLDER, songname+'.csv')
metaFile = os.path.join(THIS_FOLDER, songname+'Meta.csv')

np.savetxt(knockFile, result, delimiter=",")
np.savetxt(metaFile, metadata, delimiter=",")
