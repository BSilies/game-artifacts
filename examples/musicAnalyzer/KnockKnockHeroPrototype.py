import pygame
import os
import threading
import time

class Clapper(threading.Thread):
    def __init__(self, threadID, variant, sound, length):
        threading.Thread.__init__(self)
        self.THIS_FOLDER = os.path.dirname(os.path.abspath(__file__))
        self.threadID = threadID
        self.variant = variant
        self.sound = os.path.join(self.THIS_FOLDER, sound)
        self.length = length
        self.sequence = [1,1,1,0,1,1,0,1,0,1,1,0]
        pygame.init()
        pygame.mixer.init()
        self.sound_play = pygame.mixer.Sound(self.sound)

    def run(self):
        delay = 0.4

        running = True
        while running:
            i = 0
            while i < 2:
                for status in self.sequence:
                    if status:
                        pygame.mixer.Sound.play(self.sound_play)
                        time.sleep(delay)
                    else:
                        time.sleep(delay)
                    time.sleep(delay)
                i += 1
            if self.variant == 2:
                element = self.sequence[0]
                self.sequence.pop(0)
                self.sequence.append(element)

thread1 = Clapper(1,1,'clap1.wav',0.2)
thread2 = Clapper(2,2,'clap2.wav',0.3)

thread1.start()
thread2.start()
