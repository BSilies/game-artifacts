#include <Adafruit_NeoPixel.h>

#ifdef __AVR__
 #include <avr/power.h> // Required for 16 MHz Adafruit Trinket
#endif

// Which pin on the Arduino is connected to the NeoPixels?
// On a Trinket or Gemma we suggest changing this to 1:
#define LED_PIN 1

// How many NeoPixels are attached to the Arduino?
#define LED_COUNT 24
#define SENSOR_PIN 3

Adafruit_NeoPixel strip(LED_COUNT, LED_PIN, NEO_GRB + NEO_KHZ800);


void setup() {

  //********** CHANGE PIN FUNCTION  TO GPIO **********
  //GPIO 1 (TX) swap the pin to a GPIO.
  pinMode(1, FUNCTION_3); 
  //GPIO 3 (RX) swap the pin to a GPIO.
  pinMode(3, FUNCTION_3); 
  //**************************************************

  pinMode(SENSOR_PIN, INPUT);
  pinMode(LED_PIN, OUTPUT);

  strip.begin();           // INITIALIZE NeoPixel strip object (REQUIRED)
  strip.clear();
  strip.show();            // Turn OFF all pixels ASAP
  strip.setBrightness(50); // Set BRIGHTNESS to about 1/5 (max = 255)

  attachInterrupt(digitalPinToInterrupt(SENSOR_PIN), doKnock, RISING);
}

void loop() {
  // put your main code here, to run repeatedly:

}

ICACHE_RAM_ATTR void doKnock(){
  strip.fill(strip.Color(0,255,0));      //  Set pixel's color (in RAM)
  strip.show();
  delay(300);
  strip.clear();
  strip.show();
}
