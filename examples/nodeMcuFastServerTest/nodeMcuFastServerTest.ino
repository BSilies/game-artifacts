// Library für WiFi-Verbindung
#include <ESP8266WiFi.h>
#include <SPI.h>

// Daten des WiFi-Netzwerks
const char* ssid     = "KnockKnock";
const char* password = "herohero";

// Port des Web Servers auf 80 setzen
WiFiServer server(80);

long timer = 0L;
bool checkForButton;
int falseCounter;
int correctCounter;

// Auxiliar variables to store the current output state
String output5State = "off";

// Assign output variables to GPIO pins
const int output5 = D0;
const int inputButton = D1;

// Variable für den HTTP Request
String header;

void setup() {
  //setup console
  Serial.begin(115200);

  // Verbindung mit dem Netzwerk
  Serial.print("Connecting to WiFi");
  IPAddress ip(192, 168, 42, 10);
  IPAddress gateway(192, 168, 42, 1);
  IPAddress subnet(255, 255, 255, 0);
  WiFi.config(ip, gateway, subnet);
  WiFi.begin(ssid, password);
  //Waehrend noch nicht verbunden weiter versuchen.
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  // Ip Adresse ausgeben
  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
  //Server starten
  server.begin();
  //Output für Pin festlegen
//  pinMode(output5, OUTPUT);
//  //Button input festlegen
//  pinMode(inputButton, INPUT_PULLUP);
//  //LED einschalten
//  digitalWrite(output5, LOW);
//  //Counter initialiseren
//  //Verwendet für richtige und falsche druecker
//  correctCounter = 0;
//  falseCounter = 0;
//  //Variable, ob ein Button gedrueckt werden soll
//  checkForButton = false;

  //Interrupt initialisiern
  //attachInterrupt(digitalPinToInterrupt(inputButton), blink, CHANGE);
}

void loop() {
  WiFiClient client = server.available();

  // Wenn der Client den Server aufruft.
  if (client) {                            
    Serial.println("Client available");
    //Anfrage des Clients
    String currentLine = "";

    //Wahrend der Client verbunden ist
    while (client.connected()) {
      //Solange der Client sendet
      if (client.available()) {
        //Zeichen des Clients lesen
        char c = client.read();    
        
        // Zeichen auf der Console ausgeben
        Serial.write(c);                   
        //Zeichen dem Header hinzufügen
        header += c;
        //Wenn eine neue Zeile erreicht wird => Zeichen für Ende des Requests
        if (c == '\n') {                   

          // Leerzeile => Ende des HTTP Requests
          if (currentLine.length() == 0) {

            // Antwort an den Client senden, dass die Anfrage akzeptiert wurde.
            client.println("HTTP/1.1 200 OK");
            client.println("Content-type:text/html");
            client.println("Connection: close");
            client.println();
            
//            //Wenn die Sub-Seite "/buttonPress" aufgerufen wird.
//            if(header.indexOf("GET /buttonPress") >= 0){
//            //Timer zurueck setzen und LED Einschalten
//            timer = 0;
//            digitalWrite(output5, LOW);
//            Serial.println("Turn on pin D0...");
//            //Auf Knopfdruck warten
//            checkForButton = true;
//            }

            // Webseite uebertragen
            client.println("<!DOCTYPE html><html>");
            client.println("<head><meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">");
            client.println("<link rel=\"icon\" href=\"data:,\"></head>");
            client.println("<body><h1 align=\"center\">Hier spricht Ole :)</h1></body></html>");

            // Die Antwort mit einer Leerzeile beenden
            client.println();
            // Loop beenden
            break;
          } else {
            // Bei einer Neuen Zeile, die Variable leeren
            currentLine = "";
          }
        } else if (c != '\r') {  // alles andere als eine Leerzeile wird
          currentLine += c;      // der Variable hinzugefüht
        }
      }
    }
    //Header leeren
    header = "";
    // Verbindung beenden
    client.stop();
    Serial.println("Client disconnected");
    Serial.println("");
   
  }

  //Log Ausagben, alle 1000 Schleifen durchlaufe
//  if(timer % 10000 == 0){
//    Serial.print("Timer: ");
//    Serial.println(timer);
//    Serial.print("False counter: ");
//    Serial.println(falseCounter);
//    Serial.print("Correct counter: ");
//    Serial.println(correctCounter);
//  }
  //Error wenn 25000 Schleifendurchläufe erreicht sind.
//  if(timer >  25000 && checkForButton){
//      digitalWrite(output5, HIGH);
//      checkForButton = false;
//      falseCounter++;
//  }
//  timer++;

  
}

//Interrupt Funktion , wenn Knopf gedrückt.
ICACHE_RAM_ATTR void blink(){
  Serial.println("Blink executed");
  if(checkForButton == true){
  correctCounter++;
  checkForButton = false;
  }

}
