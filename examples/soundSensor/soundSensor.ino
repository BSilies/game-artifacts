int ledPin=3;
int sensorPin=4;
int val =0;
float timer = 10000;
int counter = 0;
void setup(){
  pinMode(ledPin, OUTPUT);
  pinMode(sensorPin, INPUT);
  Serial.begin (9600);
}
  
void loop (){
  val = digitalRead(sensorPin);
  //Serial.println (val);
  // when the sensor detects a signal above the threshold value, LED flashes
  if (val==HIGH) {
    counter = 0;
    Serial.println("Value high");
  }
 
  if(counter <= timer){
     digitalWrite(ledPin, HIGH);
     ++counter;
  } else {
     digitalWrite(ledPin, LOW);
  }
}
