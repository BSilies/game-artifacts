[&larr; Home](../README.md)

# Morsecode

Der/die Spieler/in muss einen Begriff als Morsecode eingeben. Wurde der richtige Begriff eingegeben,
erhält man einen Hinweis für das weitere Rätsel. Die Morsecode-Tabelle wird im Raum versteckt.

**Benötigt**: Schlüsselwort, das gemorst werden soll

**Liefert**: Zahlencode oder Codewort als Morsecode

## Aufbauanleitung

## Requisiten

- Morsetaster => könnte ein Tacker sein
- Lampe
- Morsecodetabelle als Wandbild oder aus Holz im raum verscheckt (CNC oder 3D-Druck)
- Geheimnachricht
- Zettel
- Stift

## Dokumentation

### Bauteile Requisiten

#### Morsetaster
- 3D-Druck (Wippe, Druckknopf, Gestell)
- Federn
- Drähte/Kabel
- Kontaktblech
- Kontaktnadel
- evtl. ein Gyroscope Sensor => Flackern der Lampe

#### Lampe
- Schreibtischlampe
- Sockel/Lampenfuß
- Relais?

#### Morsecodetabelle
- Lasercutter/CNC-Fräse
- Bilderrahmen

### Schaltung

#### Komponenten

- Widerstände
- Kabel
- Relais srd-05vdc-sl-c
- Arduino nano (China-Klon ok)
- Schreibtischlampe (muss schnelles Schalten abkönnen)
- Tacker (KEIN Mini)

#### Schaltpläne

[&larr; Home](../README.md)
