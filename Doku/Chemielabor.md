[&larr; Home](../README.md)

# Chemielabor

Der/die Spieler/in muss verschiedene Flüssigkeiten mit einem PH-Indikatorstreifen untersuchen und
die Indikatorstreifen dann in richtiger Reihenfolge in eine Box stecken.

**Benötigt**: Flüssigkeiten und Indikatorstreifen

**Liefert**: Gegenstand in einer Klappe, die sich öffnet

## Aufbauanleitung

## Requisiten

- Drei Behältnisse mit Lösungen mit verschiedenem PH-Wert.
  - Behältnisse sind bis auf kleinen Schlitz verschlossen
- Box in Optik eines Messgeräts mit Fach
- Indikatorstreifen

## Dokumentation

### Bauteile Requisiten

#### Flüssigkeitsbehältnisse

- Becher mit Deckel
  - Deckel muss Schlitz enthalten
- LEDs
- Gestell

#### Messgerät

- Kiste
- Blende mit 3 Schlitzen/Tunneln
  - Schlitze mit Zahlen (PH-Wert) beschriftet
- Grüne und Rote LED
- verriegeltes Fach

### Schaltung

#### Komponenten

- 3 Farbsensoren
- Widerstände
- Grüne und Rote LED
- Arduino
- Kabel
- Servo

#### Schaltpläne

[&larr; Home](../README.md)
