[&larr; Home](../README.md)

# Knock-Knock Hero

Die Spieler müssen als Team an verschiedenen Stationen gemeinsam zu einem Lied eine festgelegte Sequenz klopfen.

**Benötigt**: Irgendwas um ein Radio anzuschalten?

**Liefert**: Gegenstand in einer Klappe, die sich öffnet

## Aufbauanleitung

## Requisiten

- Radio mit Klappe
  - passendes Lied (They don't care about us)
- Klopfstationen

## Dokumentation

### Bauteile Requisiten

#### Radio
- Gehäuse
- Raspberry Pi
- Kabel
- Lautsprecher
- Servo

#### Klopfstation
- Grundplatte o.ä.
- Taster
- Mikrocontroller
- Kabel

### Schaltung

#### Komponenten

- Widerstände
- Kabel
- LEDs
- Schiebe- oder Kippschalter (Ausführung egal)
- Raspberry Pi
- 4 * NodeMCU V3.3 ESP8266 ESP-12 E Lua CH340
- 4 * Schallsensor mit Potentiometer [Link](https://www.conrad.de/de/p/iduino-1485297-mikrofon-schallsensor-1485297.html?hk=SEM&WT.srch=1&WT.mc_id=google_pla&s_kwcid=AL%21222%213%21367270211499%21%21%21g%21%21&ef_id=CjwKCAiA7939BRBMEiwA-hX5J2cd9vBdkvO5zRsX6QoR5O6b_x8JeTAAA4pO72V6WUKh7JZMjOXAgxoCXv0QAvD_BwE%3AG%3As&gclid=CjwKCAiA7939BRBMEiwA-hX5J2cd9vBdkvO5zRsX6QoR5O6b_x8JeTAAA4pO72V6WUKh7JZMjOXAgxoCXv0QAvD_BwE)
- sg90 Servo
- Lautsprecher (AUX) *wahrscheinlich vorhanden*

#### Schaltpläne

[&larr; Home](../README.md)
