[&larr; Home](../README.md)

# Ideenpool
- Indikatorstreifen &rarr; Lösungen mit verschiedenem PH-Wert o.ä. identifizieren ([Doku](Doku/Chemielabor.md))
- verschiedene Elemente mit RFID-Chip &rarr; Elemente richtig einsortieren
  - vernetzen um Elemente an verschiedenen Orten im Raum platzieren zu können
- Potentiometer richtig einstellen
- Steckbrett verkabeln &rarr; Stromkreis schließen
- Klopfcode um eine Tür zu öffnen.
  - Verschiedene Klopfstationen
  - "Guitar Hero" &rarr; Klopfen wenn Lampe blinkt
    - They don't care about us
    - Highway to hell
    -
- Würfel mit Gyro-Sensor &rarr; bestimmte Position gedreht werden
- Wählscheibentelefon
- mischen von Flüssigkeiten für bestimmte Farbe (Pipettieren nach Anleitung)
- Bottom-Up Glas reversed &rarr; Flüssigkeit fließt aus dem Glas heraus
- Pendel &rarr; Periode richtig einstellen
- periodisches Ereignis durchbrechen um Reset zu verhindern
- Glasprisma drehen um Licht "richtig" zu brechen
- LF-Sender und Empfänger &rarr; geheimer Radiosender *(wahrscheinlich zu analog)*
- Platten zu Molekül zusammen stecken *(wahrscheinlich zu analog)*
- Elemente im Raum zählen &rarr; Zahlenkombination eingeben
- "Spiegelpfad"

spiegel laser kiste => spiegel festhalten

[&larr; Home](../README.md)
